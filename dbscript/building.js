const mongoose = require('mongoose')
const Building = require('../models/Buildings')
mongoose.connect('mongodb://localhost:27017/example')

async function clear () {
  await Building.deleteMany({})
}

async function main () {
  await clear()
  await Building.insertMany([{
    name: 'ตึกคณะวิทยาการคอมพิวเตอร์',
    level: 11
  },
  {
    name: 'ตึกสิรินธร',
    level: 11
  }])
}

main().then(function () {
  console.log('Finish')
})
